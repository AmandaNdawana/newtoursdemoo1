from selenium import webdriver

#this line direct us to the chrom driver path
driver = webdriver.Chrome('./Drivers/Chromedriver.exe')

driver.get("http://demo.guru99.com/test/newtours/")

driver.find_element_by_xpath("//a[contains(text(),'SIGN-ON')]").click()

driver.find_element_by_xpath("//input[@name='userName']").send_keys("Test")

driver.find_element_by_xpath("//input[@name='password']").send_keys("Test")

driver.find_element_by_xpath("//input[@name='submit']").click()

Home_verification=driver.find_element_by_xpath("//h3[contains(.,'Login Successfully')]").text

if Home_verification=="Login Successfully":
    print("User logged in successfully")
    driver.close()

    assert True

else:
    print("Log in failed, The user was supposed to see:" +Home_verification)
    driver.close()
    assert False
