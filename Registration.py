import time
from select import select

from selenium import webdriver

# To get chrome driver path
#driver = webdriver.Chrome('./Drivers/chromedriver.exe')
driver=webdriver.Chrome('C:\\Users\\User\\PycharmProjects\\NewToursDemo\\Drivers\\chromedriver.exe')

#To open the website
driver.get("http://demo.guru99.com/test/newtours/")

# To click Register button
driver.find_element_by_xpath("//a[contains(text(),'REGISTER')]").click()

# To type first name
driver.find_element_by_xpath("//input[@name='firstName']").send_keys("Amanda")

# To type last name
driver.find_element_by_xpath("//input[@name='lastName']").send_keys("Manda")

# To type phone number
driver.find_element_by_xpath("//input[@name='phone']").send_keys("0815871823")

# To type the username
driver.find_element_by_xpath("//input[@id='userName']").send_keys("amanda@gmail.com")

# To type the address
driver.find_element_by_xpath("//input[@name='address1']").send_keys("66 stinkwood")

# To type the province
driver.find_element_by_xpath("//input[@name='state']").send_keys("Western cape")

# To type the postal code
driver.find_element_by_xpath("//input[@name='postalCode']").send_keys("7785")

# To select the country
# identify dropdown with Select class
sel = select(driver.find_element_by_id('country'))
#select by select_by_visible_text() method
sel.select_by_visible_text("SOUTH AFRICA")
time.sleep(0.8)
#select by select_by_index() method
sel.select_by_index(0)
driver.close()

# To type the user name
driver.find_element_by_xpath("//input[@id='email']").send_keys("amanda@gmail.com")

# To type the password
driver.find_element_by_xpath("//input[@name='password']").send_keys("Amila@2019")

# To confirm password
driver.find_element_by_xpath("//input[@name='confirmPassword']").send_keys("Amila@2019")

# To type the submit button
driver.find_element_by_xpath("//input[@name='submit']").click()

# To click sign-in link
driver.find_element_by_xpath("//a[contains(text(),'sign-in')]").click()

# To type the user name
driver.find_element_by_xpath("//input[@name='userName']").send_keys("amanda@gmail.com")

# To type the password
driver.find_element_by_xpath("//input[@name='password']").send_keys("Amila@2019")

# To type the submit button
driver.find_element_by_xpath("//input[@name='submit']").click()

SignIn_verification=driver.find_element_by_xpath("//h3[contains(.,'Login Successfully')]").text

if SignIn_verification=="Login Successfully":
    print("User logged in successfully")
    driver.close()

    assert True

else:
    print("Log in failed")
    driver.close()
    assert False
